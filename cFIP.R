## Clean the Environment

rm(list=ls(all=TRUE))

## Recommended Libraries: substitute your own database driver for RMySQL as needed

library(plyr)
library(RMySQL)
library(lme4)
library(SDMTools)

## Written to query a database; alternatively, select these categories from Retrosheet or a similar source yourself

for (s in 2015)
{
  q <- paste( "SELECT e.resp_bat_id AS batter,
              e.pit_start_fl AS role,
              e.res_pit_id AS pitcher,
              e.bat_home_id AS bat_home,
              g.home_team_id as home_team,
              e.bat_hand_cd AS stands,
              e.pit_hand_cd AS throws,
              g.park_id AS stadium,
              g.base4_ump_id as umpire,
              e.pos2_fld_id AS catcher,
              e.event_cd AS event
              FROM events e
              JOIN games g ON e.YEAR_ID=g.YEAR_ID AND e.LVL=g.LVL AND e.game_id = g.game_id
              WHERE e.year_id = ",s," AND e.lvl = 'mlb' and e.bat_event_FL = 'T'", sep="") 
  
  cFIP.data <- dbGetQuery(con1, q)
  HR.s <- cFIP.data
  BB.s <- cFIP.data
  K.s <- cFIP.data
  HBP.s <- cFIP.data
  tbf.s <- as.data.frame(table(cFIP.data$pitcher))
  tbf.s <- rename(tbf.s, replace=c("Var1" = "pitcher", "Freq" = "tbf"))
  HR.s$event<- ifelse(HR.s[,11]=='23', 1, 0)
  HR.s <- rename(HR.s, replace=c("event" = "HR"))
  BB.s$event <- ifelse(BB.s[,11]=='14', 1, 0)
  BB.s <- rename(BB.s, replace=c("event" = "BB"))
  HBP.s$event <- ifelse(HBP.s[,11]=='16', 1, 0)
  HBP.s <- rename(HBP.s, replace=c("event" = "HBP"))
  K.s$event <- ifelse(K.s[,11]=='3', 1, 0)
  K.s <- rename(K.s, replace=c("event" = "K"))
  
  #Model cFIP components
  
  ## Home Runs model
  
  HR.s.glmer <- glmer(HR ~ stands*throws + stadium + (1|batter) + (1|pitcher), data=HR.s, family=binomial(link='probit'), nAGQ=0)
  HR.s.f <-  fixef(HR.s.glmer) 
  HR.s.r <-  ranef(HR.s.glmer)
  HR.s.pitcher.r <- HR.s.r$pitcher
  HR.s.batter.r <- HR.s.r$batter
  fixed.int <- as.numeric(HR.s.f[1]  )
  HR.null <- fixed.int + median(HR.s.pitcher.r[ ,1]) + median(HR.s.batter.r[ ,1])
  HR.null.prob <- pnorm(HR.null)
  
  HR.s.pitcher.r$sum.int <- (HR.s.pitcher.r[,1] + HR.null)
  HR.s.pitcher.r$prob <- pnorm(HR.s.pitcher.r$sum.int)
  HR.s.pitcher.r$P.prob <- HR.s.pitcher.r$prob - HR.null.prob
  HR.s.pitcher.r$HR.FIP <- HR.s.pitcher.r$P.prob * 13
  
  ##BB Model
  
  BB.s.glmer <- glmer(BB ~ bat_home + stands*throws + stadium + (1|batter) + (1|pitcher) + (1|catcher) + (1|umpire), data=BB.s, family=binomial(link='probit'), nAGQ=0)
  BB.s.f <-  fixef(BB.s.glmer) 
  BB.s.r <-  ranef(BB.s.glmer)
  BB.s.pitcher.r <- BB.s.r$pitcher
  BB.s.batter.r <- BB.s.r$batter
  BB.s.catcher.r <- BB.s.r$catcher
  BB.s.umpire.r <- BB.s.r$umpire
  fixed.int <- as.numeric(BB.s.f[1]  )
  BB.null <- fixed.int + median(BB.s.pitcher.r[ ,1]) + median(BB.s.catcher.r[ ,1]) + median(BB.s.batter.r[ ,1]) + median(BB.s.umpire.r[ ,1])
  BB.null.prob <- pnorm(BB.null)
  
  BB.s.pitcher.r$sum.int <- (BB.s.pitcher.r[,1] + BB.null)
  BB.s.pitcher.r$prob <- pnorm(BB.s.pitcher.r$sum.int)
  BB.s.pitcher.r$P.prob <- BB.s.pitcher.r$prob - BB.null.prob
  BB.s.pitcher.r$BB.FIP <- BB.s.pitcher.r$P.prob * 3
  
  ##HBP Model
  
  HBP.s.glmer <- glmer(HBP ~ bat_home + stands*throws + stadium + (1|batter) + (1|pitcher) + (1|catcher), data=HBP.s, family=binomial(link='probit'), nAGQ=0)
  HBP.s.f <-  fixef(HBP.s.glmer) 
  HBP.s.r <-  ranef(HBP.s.glmer)
  HBP.s.pitcher.r <- HBP.s.r$pitcher
  HBP.s.batter.r <- HBP.s.r$batter
  HBP.s.catcher.r <- HBP.s.r$catcher
  fixed.int <- as.numeric(HBP.s.f[1]  )
  HBP.null <- fixed.int + median(HBP.s.pitcher.r[ ,1]) + median(HBP.s.batter.r[ ,1]) + median(HBP.s.catcher.r[ ,1])
  HBP.null.prob <- pnorm(HBP.null)
  
  HBP.s.pitcher.r$sum.int <- (HBP.s.pitcher.r[,1] + HBP.null)
  HBP.s.pitcher.r$prob <- pnorm(HBP.s.pitcher.r$sum.int)
  HBP.s.pitcher.r$P.prob <- HBP.s.pitcher.r$prob - HBP.null.prob
  HBP.s.pitcher.r$HBP.FIP <- HBP.s.pitcher.r$P.prob * 3
  
  #K Model
  
  K.s.glmer <- glmer(K ~ bat_home + stands*throws + stadium + (1|batter) + (1|pitcher) + (1|catcher) + (1|umpire), data=K.s, family=binomial(link='probit'), nAGQ=0)
  K.s.f <-  fixef(K.s.glmer) 
  K.s.r <-  ranef(K.s.glmer)
  K.s.pitcher.r <- K.s.r$pitcher
  K.s.batter.r <- K.s.r$batter
  K.s.catcher.r <- K.s.r$catcher
  K.s.umpire.r <- K.s.r$umpire
  fixed.int <- as.numeric(K.s.f[1]  )
  K.null <- fixed.int + median(K.s.pitcher.r[ ,1]) + median(K.s.catcher.r[ ,1]) + median(K.s.batter.r[ ,1]) + median(K.s.umpire.r[ ,1])
  K.null.prob <- pnorm(K.null)
  
  K.s.pitcher.r$sum.int <- (K.s.pitcher.r[,1] + K.null)
  K.s.pitcher.r$prob <- pnorm(K.s.pitcher.r$sum.int)
  K.s.pitcher.r$P.prob <- K.s.pitcher.r$prob - K.null.prob
  K.s.pitcher.r$K.FIP <- K.s.pitcher.r$P.prob * 2
  
  ##Final Contextual FIP Aggregation, 100-scale, minus-metric, with forced SD of 15
  
  HR.s.pitcher.r$playerid <- rownames(HR.s.pitcher.r)
  cFIP.s <- data.frame(HR.s.pitcher.r$playerid)
  cFIP.s <- rename(cFIP.s, replace=c("HR.s.pitcher.r.playerid" = "pitcher"))
  cFIP.s$BB <- BB.s.pitcher.r$BB.FIP
  cFIP.s$HR <- HR.s.pitcher.r$HR.FIP
  cFIP.s$HBP <- HBP.s.pitcher.r$HBP.FIP
  cFIP.s$K <- K.s.pitcher.r$K.FIP
  cFIP.s$tbf <- tbf.s$tbf
  cFIP.s$Agg <- cFIP.s[,2] + cFIP.s[,3] + cFIP.s[,4] - cFIP.s[,5]
  cFIP.s$Agg_Z <- ((cFIP.s$Agg - wt.mean(cFIP.s$Agg, cFIP.s$tbf)) / wt.sd(cFIP.s$Agg, cFIP.s$tbf))
  cFIP.s$cFIP <- 100 + cFIP.s$Agg_Z * 15
  cFIP.s$year_id <- s
  
  ## Merge into a master player database to get names of players
  
  print(s)
  
}
dbDisconnect(con1)